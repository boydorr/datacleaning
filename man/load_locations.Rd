% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/load_locations.R
\name{load_locations}
\alias{load_locations}
\title{Load in Location Spreadsheets}
\usage{
load_locations(pathway)
}
\arguments{
\item{pathway}{This is the path for the base location of where all the files
are stored.}
}
\value{
A single dataframe containing all of the individual location sheets
for each project bound together with a location tag.
}
\description{
Loads in every location spreadsheet located on the Boyd Orr
server (providing it has "-Location.csv" at the end of the name string) and
binds all to a single dataframe.
}
\details{
This function reads in each spreadsheet individually and attaches
the location tag in the same format as the \code{load_metadata} function:
"Project.Name--Location.File.Name".
}
